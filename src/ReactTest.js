import React, { Component } from 'react';
import { AppRegistry, Text, View } from 'react-native';

class ButtonList extends Component{
	render() {
    return (
      <View style={{alignItems: 'center'}}>
        <Button text='$100 - $500' />
        <Button textd='$500 - $1000' />
        <Button text='$1000 - $2500' />
        <Button text='$2500 - $5000' />
      </View>
    );
  }
}

class Button extends Component{
	render() {
		return (
			<View style={{backgroundColor:'#00FF00'}}>
				<Text style={{margin: 25, color:'#aaaaaa'}}>{this.props.text}</Text>
				</View>
		);
	}
}

AppRegistry.registerComponent('testProjectReact', () => ButtonList);
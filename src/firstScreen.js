import React, { Component} from 'react';
import { AppRegistry, Text, View, TextInput, DatePickerAndroid, Button } from 'react-native';
import {StackNavigator} from 'react-navigation'

var component2Test = require('./component2');

class TestComponent extends Component{
	constructor(props) {
	    super(props);
	    this.state = new viewModel(this);
		this.navigate = function() {
			this.props.navigation.navigate('Additional', {name: 'Lucy'});
			  // title: 'next',
			  // component: Component2,
			  // passProps: {listings: 'hello'}
		}

	}

	render() {
	  return (
	  	testComponentView(this)
	    );
	 }

}

testComponentView = function (context) { 
	return (
		<View style={{margin: 25}}>
			<Text >{context.state.title}</Text>
			<TextInput onChange={(event)=>context.state.onTitleChanged(event.nativeEvent.text)}/>
			<Button title="showPicker" onPress={context.state.showPicker.bind(
				context, 'spinner', {date: context.state.spinnerDate, mode: 'spinner'})}/>
			<Button  style={{margin: 50}} title="test Navigator" onPress={context.navigate.bind(context)}/>
		</View>

	)
}

function viewModel(context) {
	var self = this;
	self.title = 'Егор';
	self.onTitleChanged=function(text){
		context.setState({title: text})
	};
	self.spinnerDate=new Date(2020, 4, 5);

	self.showPicker = async (stateKey, options) => {
	  	try {
	      var newState = {};
	      const {action, year, month, day} = await DatePickerAndroid.open(options);
	      if (action === DatePickerAndroid.dismissedAction) {
	        newState[stateKey + 'Text'] = 'dismissed';
	      } else {
	        var date = new Date(year, month, day);
	        newState[stateKey + 'Text'] = date.toLocaleDateString();
	        newState[stateKey + 'Date'] = date;
	      }
	      this.setState(newState);
	    } catch ({code, message}) {
	      console.warn(`Error in example '${stateKey}': `, message);
	    }
	};

 };

module.exports = TestComponent;
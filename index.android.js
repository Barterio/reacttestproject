import React, { Component} from 'react';
// import React from 'react-native-deprecated-custom-components'
import { AppRegistry, Text, View, TextInput, Button } from 'react-native';
import {StackNavigator} from 'react-navigation';
// import firstScreen from './src/firstScreen';
// import component2 from './src/component2';
var firstScreen = require('./src/firstScreen');
var component2 =require('./src/component2');

const BasicApp = StackNavigator({
  Main: {screen: firstScreen},
  Additional: {screen: component2},
});

AppRegistry.registerComponent('testProjectReact', () => BasicApp);